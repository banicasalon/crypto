<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public function index()
	{
		//$this->homepage();
		$this->encryption->encryption();
	}
	//Login Page
	public function homepage() {	
		$this->load->view('template/header/header-login');
		$this->load->view('main');
		$this->load->view('template/footer/footer-login');
	}
	//Registration Page
	public function register() {
		$this->load->view('template/header/header-login');
		$this->load->view('regestration');
		$this->load->view('template/footer/footer-login');
	}
	//Landing Page
	public function landing(){
		$this->dashboard();
	}
	public function dashboard() {		
		if($this->session->userdata('is_logged_in'))
		{
		$id = $this->session->userdata('id');
		$this->load->model('user');
		$data['account'] = $this->user->get_account_info($id);
			$this->load->view('template/header/header-dashboard');
			$this->load->view('template/navigation',$data);
			$this->load->view('dashboard');
			$this->load->view('template/footer/footer-dashboard');
		}else{ redirect('app/homepage');}
	}
	
	
	
	public function inbox() {
		if($this->session->userdata('is_logged_in')){
			$this->load->view('inbox');	
		}
		else{ redirect('app/homepage');}

	}
	public function sent() {
		if($this->session->userdata('is_logged_in')){
		$this->load->view('sent');
		}else{ redirect('app/homepage');}
	}
	public function message() {
		if($this->session->userdata('is_logged_in')){
		$this->load->view('message');
		}else{ redirect('app/homepage');}
	}
	public function view() {
		if($this->session->userdata('is_logged_in')){
		$this->load->view('view');
		}else{ redirect('app/homepage');}
	}
	public function get()
	{
		$id = $this->session->userdata('id');
		$this->load->model('user');
		$data = $this->user->get_account_info($id);
		/*$data = array(
    array( "name" => "smith", "age" => "20", "city" => "adelade", "country" => "australia"),
    array("name" => "john", "age" => "20", "city" => "parth", "country" => "australia" ),
    array("name" => "david", "age" => "20", "city" => "london", "country" => "england" )
    );*/
		echo json_encode($data);
	}
	public function getMsg()
	{
		$email = $this->session->userdata('email');
		$this->load->model('user');
		$data = $this->user->get_msg($email);
		//var_dump($data);
	/*$data = array(
    array( "name" => "smith", "age" => "20", "city" => "adelade", "country" => "australia"),
    array("name" => "john", "age" => "20", "city" => "parth", "country" => "australia" ),
    array("name" => "david", "age" => "20", "city" => "london", "country" => "england" )
    );*/
		echo json_encode($data);
	}
	public function getSent()
	{
		$email = $this->session->userdata('email');
		$this->load->model('user');
		$data = $this->user->get_msg($email);
		//var_dump($data);
	/*$data = array(
    array( "name" => "smith", "age" => "20", "city" => "adelade", "country" => "australia"),
    array("name" => "john", "age" => "20", "city" => "parth", "country" => "australia" ),
    array("name" => "david", "age" => "20", "city" => "london", "country" => "england" )
    );*/
		echo json_encode($data);
	}
//-----------------
	//validation and credentials and log out
	//-----------------------------
	# validate the users credentials
	function login_validation()
	{
		
		$this->load->model('model_session');
		// load the form validation library.
		$this->load->library('form_validation');
		$this->model_session->activate($this->input->post('email'));
		// setting up some rules
		// parameters: 1 = input name, 2 = the Name, 3 = the rules
		$this->form_validation->set_rules('email', 'Email', 'required|trim|callback_validate_credentials');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		
		#if the form validation  runs
		if($this->form_validation->run())
		{
			$q = $this->model_session->getall($this->input->post('email'));

			foreach ($q as $key) {
				
				$data = array(
					// 'name' => $this->input->post('name'),
					// 'id' => $key->client_id,
					'f_name' => $key->f_name,
					// 'mname' => $key->client_mname,
					// 'lname' => $key->client_lname,
					// 'address' => $key->client_address,
					// 'email' => $key->client_email,
					// 'cell' => $key->client_cell_num,
					// 'tell' => $key->client_tell_num,
					// 'pic' => $key->file_name,
					'id' => $key->id,
					'email' => $key->email,
					'password' => $key->password,
					'is_logged_in' => 1
				);
			}
			
			$this->session->set_userdata($data);
			 redirect('app/landing');
			//var_dump($data['is_logged_in']);
		}
		else{
			redirect('app/homepage');
		}
		
	}
	// it is called by callback function in the login_validation function
	public function validate_credentials($data)
	{
		// load a model
		$this->load->model('user');
		$data = array(
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password')
			);
		$query	= $this->user->login($data);
		var_dump($query);
		// check the model's function
		if($query){
			return true;
		}else{
			//$this->form_validation->set_message('validate_credentials', 'incorrect email/password');
			return false;
		}
	}
		public function logout()
	{
		$this->session->sess_destroy();
		$this->homepage();
	}
	/*register user*/
	

/*	function register_validation()
	{
		// load the form validation library.
		$this->load->library('form_validation');
		// setting up some rules
		// parameters: 1 = input name, 2 = the Name, 3 = the rules
		$this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique|callback_register_validate');
		
		#if the form validation  runs
		if($this->form_validation->run())
		{
		
			 redirect('app/landing');
			//var_dump($data['is_logged_in']);
		}
		else{
			redirect('app/homepage');
		}
		
	}*/
	/*public function register_validate()
	{

 		$this->load->model('user');
		$test = $this->input->post('emailadd');
		$test2 = $this->user->check_email($test);
		var_dump($test2);
		if ($test2 == true) {
		//var_dump($test);
		$data['message'] = "Email Existed. Try again!!";
		$this->load->view('template/header/header-login');
		$this->load->view('main',$data);
		$this->load->view('template/footer/footer-login');
		}else{
			$query = $this->user->reg_validation();
		//var_dump($test2);
		}	
	}*/public function register_validate()
	{

 		$this->load->model('user');
		/*$test = $this->input->post('emailadd');
		$test2 = $this->user->check_email($test);
		//var_dump($test2);
		if ($test2 != $test) {
			$data['message'] = "Email Existed. Try again!!";
		$this->load->view('template/header/header-login');
		$this->load->view('main',$data);
		$this->load->view('template/footer/footer-login');
		}else{*/
			$query = $this->user->reg_validation();

		if($query){
			//
	//Generate Key
	$openssllocation = 'C:/OpenSSL-Win32/bin/openssl.cfg';
	$privatekeydirectory = 'C:/Users/user/Favorites/Downloads/d';
	$path = 'C:/wamp/www/mysite_al';
	$fname = $this->input->post('fname');
	$privkey = openssl_pkey_new(array(  //Generates A new private key
              'config' => $openssllocation,
              'digest_alg' => 'des3',
              'private_key_type' => OPENSSL_KEYTYPE_RSA,
              'private_key_bits' => 1024));
              
	$de = array (
              'config' => $openssllocation,
              'digest_alg' => 'des3',
              'private_key_type' => OPENSSL_KEYTYPE_RSA,
              'private_key_bits' => 1024);
// var_dump($publickeydirectory);
openssl_pkey_export_to_file ($privkey, $privatekeydirectory, $fname, $de);
//Gets an exportable representation of a key into a file
openssl_pkey_export ($privkey, $res1, $fname,$de);
//Gets an exportable representation of a key into a string
$res = openssl_get_privatekey ($res1, $fname);

//Get a private key
$keyDetails = openssl_pkey_get_details ($privkey);
//This function returns the key details (bits, key, type).
		// $parts = explode('/', $publickeydirectory);
  //       $file = array_pop($parts);
  //       $dir = '';
  //       foreach($parts as $part)
  //           if(!is_dir($dir .= "/$part")) mkdir($dir);
		$publicPath = $path . '/publicKeys';
		$privatePath = $path . '/privKeys';
		var_dump($fname);
		$publicKey = $fname . 'pubKey.key';
		$privateKey = $fname . 'privKey.key';
        file_put_contents("$publicPath/$publicKey", $keyDetails['key']);
        file_put_contents("$privatePath/$privateKey", $res1);
// file_put_contents ($publickeydirectory,$keyDetails['key']); //Write a string to a file
// var_dump($keyDetails['key']);
		// $config = array(
		// 	'config' => 'C:\OpenSSL-Win32\bin\openssl.cfg',
		//     'digest_alg' => 'des3',
		//     'private_key_bits' => 1024,
		//     'private_key_type' => OPENSSL_KEYTYPE_RSA,
		// );
// Create the private and public key
// $res = openssl_pkey_new($config);


// $privKey = openssl_pkey_new($config);
// Extract the private key from $res to $privKey
// openssl_pkey_export($res, $privKey);

// Extract the public key from $res to $pubKey
// $pubKey = openssl_pkey_get_details($res);
// $pubKey = $pubKey["key"];
// var_dump($pubKey);
// $data = $this->input->post('file');

// Encrypt the data to $encrypted using the public key
// openssl_public_encrypt($data, $encrypted, $pubKey);

// Decrypt the data using the private key and store the results in $decrypted
// openssl_private_decrypt($encrypted, $decrypted, $privKey);

 	//echo $decrypted;
      // var_dump($decrypted);
 //END of Generate key
			//
		$data['message'] = "Successfully registered";
		$this->load->view('template/header/header-login');
		$this->load->view('main',$data);
		$this->load->view('template/footer/footer-login');
			}else{
			
			 $data['message'] = "Not registered";
		$this->load->view('template/header/header-login');
		$this->load->view('main',$data);
		$this->load->view('template/footer/footer-login');
		}
	/*	}*/
		
		
	}
	
	// send message part
		public function send() {
		//var_dump($this->input->post('date'));
		$this->load->model('user');
		$data = array(
				'to_e' => $this->input->post('to'),
				'from_e' => $this->input->post('from'),
				'date' => $this->input->post('date'),
				'sub' => $this->input->post('sub'),
				'msg' => $this->input->post('msg')
			);

		//var_dump($data);
		

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|txt|doc|docs|pdf|';
		$config['max_size']	= '10000';

		$this->load->library('upload', $config);

		$query	= $this->user->sendMsg($data);
		redirect('app/landing');
	}
public function encrypt(){
	 
	}
	//Uploading a file
	function upload(){
		$this->load->view('upload_form');
	}
	function do_upload()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|txt|doc|docs|pdf|';
		$config['max_size']	= '10000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			$this->load->view('upload_success', $data);
		}
	}
}
