<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index() {
		$this->admin_dashboard();
	}

	public function admin_dashboard() {
			$this->load->view('parts/dash-header');
			$this->load->view('parts/navigation');
			$this->load->view('admin/admin');
			$this->load->view('parts/footer');
		
	}

	

}