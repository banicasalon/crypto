<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {
	# verify if the user is can login
		function login($data)
		{
			// the first parameter is the name of the column and the second parameter is the input name...
			$this->db->where('email', $this->input->post('email'));
			$this->db->where('password', $this->input->post('password'));
			
			// get the table and stores it in a variable
			$query = $this->db->get('user');
			
			if($query->num_rows == 1){
				return  true;
			}else{
				return false;
			}		
	}
	#verify if the user has existing account
	public function check_email($test){
		$this->db->select('email');
		$this->db->from('user');
		$this->db->where('email',$test);

		$query = $this->db->get();

		if($query->num_rows() == TRUE ){
			return true;
		}else{ return false;}	
	}
	public function reg_validation()
	{
		$data = array(
		   'f_name' => $this->input->post('fname'),
		   'm_name' => $this->input->post('mname'),
		   'l_name' => $this->input->post('lname'),
		   'password' => $this->input->post('pswd'),
		   'email' =>$this->input->post('emailadd')
		);
		
		$query = $this->db->insert('user', $data); 
		
		if($query == TRUE ){
			return true;
		}else{ return false;}

	
	}
		/*public function reg_validation()
	{
		$data = array(
		   'first_name' => $this->input->post('fname'),
		   'last_name' => $this->input->post('lname'),
		   'address' => $this->input->post('address'),
		   'email' =>$this->input->post('emailadd'),
		   'birthday' =>$this->input->post('bod'),
		   'sex' =>$this->input->post->('sex'),
		   'phone_#' => $this->input->post->('pnum'),
		   'telephone_#' => $this->input->post->('tnum'),
		   'password' =>$this->input->post->('password'),
		   'registerDate' =>$this->input->post('regDate')

		);
		
		$query = $this->db->insert('user', $data); 
		
		if($query == TRUE ){
			return true;
		}else{ return false;}
	}*/
	function get_account_info($id)
		{
			$this->db->select('*');
			$this->db->from('user');
			// $this->db->join('tbl_client', 'account_id_link = account_id');
			$this->db->where('id', $id);
			$query = $this->db->get();

			return $query->result();	
		}
	function get_msg($email)
		{
			$this->db->select('*');
			$this->db->from('message');
			// $this->db->join('tbl_client', 'account_id_link = account_id');
			$this->db->where('to_e', $email);
			$query = $this->db->get();

			return $query->result();	
		}
	public function sendMsg($data)
	{
		
		$query = $this->db->insert('message', $data); 
		
		if($query == TRUE ){
			return true;
		}else{ return false;}
	}


}
