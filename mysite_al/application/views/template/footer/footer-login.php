

<script type="text/javascript" src="<?php echo base_url('assets/js/ui-bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/angular-route.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/myapp.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/angular-multiple-file-upload.js'); ?>"></script>

<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url('');?>assets/assets/plugins/jquery-1.8.2/jquery-1.8.2.min.js"></script>
	<script src="<?php echo base_url('');?>assets/assets/plugins/jquery-ui-1.10.4/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url('');?>assets/assets/plugins/bootstrap-3.2.0/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url('');?>assets/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url('');?>assets/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url('');?>assets/assets/js/login-v2.demo.min.js"></script>
	<script src="<?php echo base_url('');?>assets/assets/js/apps.min.js"></script>
	<script src="assets/plugins/password-indicator/js/password-indicator.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="<?php echo base_url('');?>assets/assets/plugins/parsley/dist/parsley.js"></script>
	<script src="<?php echo base_url('');?>assets/assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
	<script src="<?php echo base_url('');?>assets/assets/js/form-wizards-validation.demo.min.js"></script>
	<script src="<?php echo base_url('');?>assets/assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	<script>
		$(document).ready(function() {
			App.init();
			LoginV2.init();
			FormWizardValidation.init();
		});
	</script>
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','<?php echo base_url('');?>assets/assets/js/analytics.js','ga');
    
      ga('create', 'UA-53034621-1', 'auto');
      ga('send', 'pageview');
    
    </script>
</body>
</html>