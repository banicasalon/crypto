<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" ng-app="app">
<!--<![endif]-->

<!-- Mirrored from www.seantheme.com/color-admin-v1.3/page_without_sidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 24 Oct 2015 00:34:15 GMT -->
<head>
	<meta charset="utf-8" />
	<title>Crypto System | Multilayered Encryption</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="<?php echo base_url('');?>assets/assets/cssff98.css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="<?php echo base_url('');?>assets/assets/plugins/jquery-ui-1.10.4/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url('');?>assets/assets/plugins/bootstrap-3.2.0/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url('');?>assets/assets/plugins/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" />
	<link href="<?php echo base_url('');?>assets/assets/css/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url('');?>assets/assets/css/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url('');?>assets/assets/css/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url('');?>assets/assets/css/theme/default.css" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?php echo base_url('');?>assets/assets/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/angular.min.js'); ?>"></script>
</head>
<body>