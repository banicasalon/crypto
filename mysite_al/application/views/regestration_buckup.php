  <!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><span class="spinner"></span></div>
  <!-- end #page-loader -->
  
  <div class="login-cover">
      <div class="login-cover-image"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-5.jpg" data-id="login-cover-image" alt="" /></div>
      <div class="login-cover-bg"></div>
  </div>
  <!-- begin #page-container -->
  <div id="page-container" class="fade">
      </br>
      <div class="col-md-2">  </div>  
      <div class="col-md-8">     
            <div class="row">
                <!-- begin col-12 -->
          <div class="col-md-12">
              <!-- begin panel -->
                    <div class="panel panel-inverse">
                      
                        <div class="panel-body">
                            <form action="http://www.seantheme.com/" method="POST" data-parsley-validate="true" name="form-wizard">
                <div id="wizard">
                  <ol>
                    <li>
                        Identification 
                        <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
                    </li>
                    <li>
                        Contact Information
                        <small>Aliquam bibendum felis id purus ullamcorper, quis luctus leo sollicitudin.</small>
                    </li>
                    <li>
                        Login
                        <small>Phasellus lacinia placerat neque pretium condimentum.</small>
                    </li>
                    <li>
                        Completed
                        <small>Sed nunc neque, dapibus non leo sed, rhoncus dignissim elit.</small>
                    </li>
                  </ol>
                  <!-- begin wizard step-1 -->
                  <div class="wizard-step-1">
                                        <fieldset>
                                            <legend class="pull-left width-full">Identification</legend>
                                            <!-- begin row -->
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                          <div class="form-group block1">
                            <label>First Name</label>
                            <input type="text" name="firstname" placeholder="John" class="form-control" data-parsley-group="wizard-step-1" required />
                          </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                          <div class="form-group">
                            <label>Middle Initial</label>
                            <input type="text" name="middle" placeholder="A" class="form-control" data-parsley-group="wizard-step-1" required />
                          </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                          <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" name="lastname" placeholder="Smith" class="form-control" data-parsley-group="wizard-step-1" required />
                          </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>
                                            <!-- end row -->
                    </fieldset>
                  </div>
                  <!-- end wizard step-1 -->
                  <!-- begin wizard step-2 -->
                  <div class="wizard-step-2">
                    <fieldset>
                      <legend class="pull-left width-full">Contact Information</legend>
                                            <!-- begin row -->
                                            <div class="row">
                                                <!-- begin col-6 -->
                                                <div class="col-md-6">
                          <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone" placeholder="1234567890" class="form-control" data-parsley-group="wizard-step-2" data-parsley-type="number" required />
                          </div>
                                                </div>
                                                <!-- end col-6 -->
                                                <!-- begin col-6 -->
                                                <div class="col-md-6">
                          <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" name="email" placeholder="someone@example.com" class="form-control" data-parsley-group="wizard-step-2" data-parsley-type="email" required />
                          </div>
                                                </div>
                                                <!-- end col-6 -->
                                            </div>
                                            <!-- end row -->
                    </fieldset>
                  </div>
                  <!-- end wizard step-2 -->
                  <!-- begin wizard step-3 -->
                  <div class="wizard-step-3">
                    <fieldset>
                      <legend class="pull-left width-full">Login</legend>
                                            <!-- begin row -->
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <div class="controls">
                                                            <input type="text" name="username" placeholder="johnsmithy" class="form-control" data-parsley-group="wizard-step-3" required />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Pasword</label>
                                                        <div class="controls">
                                                            <input type="password" name="password" placeholder="Your password" class="form-control" data-parsley-group="wizard-step-3" required />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Confirm Pasword</label>
                                                        <div class="controls">
                                                            <input type="password" name="password2" placeholder="Confirmed password" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col-6 -->
                                            </div>
                                            <!-- end row -->
                                        </fieldset>
                  </div>
                  <!-- end wizard step-3 -->
                  <!-- begin wizard step-4 -->
                  <div>
                      <div class="jumbotron m-b-0">
                                            <h1>Login Successfully</h1>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat commodo porttitor. Vivamus eleifend, arcu in tincidunt semper, lorem odio molestie lacus, sed malesuada est lacus ac ligula. Aliquam bibendum felis id purus ullamcorper, quis luctus leo sollicitudin. </p>
                                            <p><a class="btn btn-success btn-lg" role="button">Proceed to User Profile</a></p>
                                        </div>
                  </div>
                  <!-- end wizard step-4 -->
                </div>
              </form>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
      </div>
        
      
     
  </div>
  <!-- end page container -->



  <!-- oldcopy for registration -->

    <!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><span class="spinner"></span></div>
  <!-- end #page-loader -->
  
  <div class="login-cover">
      <div class="login-cover-image"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-5.jpg" data-id="login-cover-image" alt="" /></div>
      <div class="login-cover-bg"></div>
  </div>
  <!-- begin #page-container -->
  <div id="page-container" class="fade">
      <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated flipInX" style="margin-top:75px;">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> Register
                  <small>Multilayered Encryption Sytem</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end brand -->
            <div class="login-content">
                <form action="<?php echo base_url();?>app/register_validate" method="POST" class="margin-bottom-0">
                  <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" name="fname" placeholder="First Name" />
                    </div>
                     <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" name="mname" placeholder="Middle Name" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" name="lname" placeholder="Last Name" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" placeholder="Gender" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" placeholder="Birthday" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" placeholder="Address" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" placeholder="Phone Number" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" placeholder="Telephone Number" />
                    </div>

                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" name="emailadd" placeholder="Email Address" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="password" class="form-control input-lg" name="pswd" placeholder="Password" />
                    </div>
                    
                    <div class="checkbox m-b-20">
                        <label>
                            <input type="checkbox" /> Accept Terms and Aggreements
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Sign me up</button>
                    </div>
                    <div class="m-t-20">
                        Already have account ? Click <a href="<?php echo base_url('app/homepage');?>">here</a> to sign in.
                    </div>
                </form>
            </div>
        </div>
        <!-- end login -->
        
      
     
  </div>
  <!-- end page container -->