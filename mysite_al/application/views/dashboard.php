 <!-- begin #content -->
    <div id="content" class="content" ng-controller="TabsDemoCtrl">
    
      <div class="p-20">
      <!-- begin row -->
      <div class="row">
          <!-- begin col-2 -->
          <div class="col-md-2" >
              <form>
                  <div class="input-group m-b-15">
                            <input type="text" class="form-control input-sm input-white" placeholder="Search Mail" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-inverse" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
              </form>
              <div class="hidden-sm hidden-xs">
                        <h5 class="m-t-20">Email</h5>
                        <ul class="nav nav-pills nav-stacked nav-inbox">
                            <!-- <li class="active">
                                <a href="#">
                                    <i class="fa fa-inbox fa-fw m-r-5"></i> Inbox (10)
                                </a>
                            </li> -->
                            <li class="active"><a ng-click="goToInbox()" style="cursor:pointer;"><i class="fa fa-inbox fa-fw m-r-5"></i> Inbox (10)</a></li>
                            <li><a ng-click="goToSent()" style="cursor:pointer;"><i class="fa fa-inbox fa-fw m-r-5"></i> Sent</a></li>
                            <li><a href="#"><i class="fa fa-pencil fa-fw m-r-5"></i> Draft</a></li>
                            <li><a href="#"><i class="fa fa-trash-o fa-fw m-r-5"></i> Trash</a></li>
                            <li><a href="#"><i class="fa fa-star fa-fw m-r-5"></i> Archive</a></li>
                        </ul>
                        <h5 class="m-t-20">Folders</h5>
                        <ul class="nav nav-pills nav-stacked nav-inbox">
                            <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Newsletter</a></li>
                            <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Friend</a></li>
                            <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Company</a></li>
                            <li><a href="#"><i class="fa fa-folder fa-fw m-r-5"></i> Downloaded</a></li>
                        </ul>
                    </div>
                </div>
          <!-- end col-2 -->
          <!-- begin col-10 -->
          <div class="col-md-10">
                    <div class="email-btn-row hidden-xs">
                        <a ng-click="goToMsg()" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> New</a>
                        <a href="#" class="btn btn-sm btn-default disabled">Reply</a>
                        <a href="#" class="btn btn-sm btn-default disabled">Delete</a>
                        <a href="#" class="btn btn-sm btn-default disabled">Archive</a>
                        <a href="#" class="btn btn-sm btn-default disabled">Junk</a>
                        <a href="#" class="btn btn-sm btn-default disabled">Swwep</a>
                        <a href="#" class="btn btn-sm btn-default disabled">Move to</a>
                        <a href="#" class="btn btn-sm btn-default disabled">Categories</a>
                    </div>
              <div class="email-content">
                         <div ng-view=""></div>
              </div>
          </div>
          <!-- end col-10 -->
      </div>
      <!-- end row -->
      </div>
    </div>
    <!-- end #content -->
    
     
    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
  </div>
  <!-- end page container -->