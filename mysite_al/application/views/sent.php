
	 	
	<div ng-controller="sentCtrl">
		
   <p>SENT</p>
    <?php //var_dump($data) ?>
      
  
	
	<div class="email-content">
                        <table class="table table-email">
                            <thead>
                                <tr>
                                    <th class="email-select"><a href="#" data-click="email-select-all"><i class="fa fa-square-o fa-fw"></i></a></th>
                                    <th colspan="2">
                                        <div class="dropdown">
                                            <a ng-click="goToInbox()" class="email-header-link" data-toggle="dropdown">View All <i class="fa fa-angle-down m-l-5"></i></a>
                                            <ul class="dropdown-menu">
                                                <li class="active"><a href="#">All</a></li>
                                                <li><a ng-click="goToInbox()">Unread</a></li>
                                                <li><a ng-click="goToInbox()">Contacts</a></li>
                                                <li><a ng-click="goToInbox()">Groups</a></li>
                                                <li><a ng-click="goToInbox()">Newsletters</a></li>
                                                <li><a ng-click="goToInbox()">Social updates</a></li>
                                                <li><a ng-click="goToInbox()">Everything else</a></li>
                                            </ul>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="dropdown">
                                            <a ng-click="goToInbox()" class="email-header-link" data-toggle="dropdown">Arrange by <i class="fa fa-angle-down m-l-5"></i></a>
                                            <ul class="dropdown-menu">
                                                <li class="active"><a href="#">Date</a></li>
                                                <li><a ng-click="goToInbox()">From</a></li>
                                                <li><a ng-click="goToInbox()">Subject</a></li>
                                                <li><a ng-click="goToInbox()">Size</a></li>
                                                <li><a ng-click="goToInbox()">Conversation</a></li>
                                            </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="user in users" class="messagesList">
                                    <td class="email-select"><a ng-click="goToInbox()" data-click="email-select-single"><i class="fa fa-square-o fa-fw"></i></a></td>
                                    <td class="email-sender">To:
                                        {{user.to_e}} 
                                    </td>
                                    <td style="width: 67%;" class="email-subject">
                                        <a href="#" class="email-btn" data-click="email-archive"><i class="fa fa-folder-open"></i></a>
                                        <a href="#" class="email-btn" data-click="email-remove"><i class="fa fa-trash-o"></i></a>
                                        <a href="#" class="email-btn" data-click="email-highlight"><i class="fa fa-flag"></i></a> 
                                        Subject:<a href="#" style="text-decoration:none;">  {{user.sub}}</a>
                                    </td>
                                    <td class="email-date">{{user.date}}</td>
                                </tr>
                              </tbody>
                        </table>
                        <div class="email-footer clearfix">
                            737 messages
                            <ul class="pagination pagination-sm m-t-0 m-b-0 pull-right">
                                <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-double-left"></i></a></li>
                                <li class="disabled"><a href="javascript:;"><i class="fa fa-angle-left"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-angle-right"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </div>
			        </div>
			    </div>
						
			
	