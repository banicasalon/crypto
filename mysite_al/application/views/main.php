
<!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><span class="spinner"></span></div>
  <!-- end #page-loader -->
  
  <div class="login-cover">
      <div class="login-cover-image"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-6.jpg" data-id="login-cover-image" alt="" /></div>
      <div class="login-cover-bg"></div>
  </div>
  <!-- begin #page-container -->
  <div id="page-container" class="fade">
      <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated flipInX"  style="margin-top:75px;">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> Crypto System
                    <small>Multilayered Encryption Sytem</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end brand -->
            <div class="login-content">
                <form action="<?php echo base_url('app/login_validation');?>" method="POST" class="margin-bottom-0">
                                              
                    <div class="form-group m-b-20">
                        <?php if(isset($message)) echo "<h3><font color='white'>$message</font></h3>"; ?> 
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" name="email" placeholder="Email Address" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" name="password" placeholder="Password" />
                    </div>
                    <div class="checkbox m-b-20">
                        <label>
                            <input type="checkbox" /> Remember Me
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
                    </div>
                    <div class="m-t-20">
                        Not a member yet? Click <a href="<?php echo base_url('app/register');?>">here</a> to register.
                    </div>
                </form>
            </div>
        </div>
        <!-- end login -->
   <!--      
        <ul class="login-bg-list">
            <li class="active"><a href="#" data-click="change-bg"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-1.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-2.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-3.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-4.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-5.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="<?php echo base_url('');?>assets/assets/img/login-bg/bg-6.jpg" alt="" /></a></li>
        </ul> -->
     
  </div>
  <!-- end page container -->
  
  