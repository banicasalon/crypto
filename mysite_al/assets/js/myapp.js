
var myapp = angular.module('app', ['ui.bootstrap','ngRoute', 'fileUpload']);


myapp.config(function ($routeProvider) {
 
    $routeProvider

    .when("/", {
        controller: "inboxCtrl",
        templateUrl: "http://localhost/mysite_al/App/inbox"
    })

    .when("/message", {
        controller: "msgCtrl",
        templateUrl: "http://localhost/mysite_al/App/message"
    })
    .when("/sent", {
        controller: "sentCtrl",
        templateUrl: "http://localhost/mysite_al/App/sent"
    })
    .when("/view", {
        controller: "viewCtrl",
        templateUrl: "http://localhost/mysite_al/App/view"
    })


    $routeProvider.otherwise({ redirectTo: "/" });
 
});

myapp.controller('msgCtrl', function($scope, $http) {
    $scope.name = 'Amit';
    
  $scope.users = [];
    
  $http({
    url: 'http://localhost/mysite_al/app/get',
    method: 'POST',
  }).success(function (data) {
    $scope.users = data;
  });
    console.log($scope.users); 
//this is for the date format and current date
 $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();
  $scope.maxDate = new Date(2020, 5, 22);

  

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.formats = ['MM/dd/yyyy','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];


    
});

myapp.controller('inboxCtrl', function($scope, $http, $window) {
  $scope.goToView = function() {
        $window.location.href = "http://localhost/mysite_al/app/dashboard#/view";
      }
  $scope.name = 'aljim salem';
  $scope.users = [];
     
  $http({
    url: 'http://localhost/mysite_al/app/getMsg',
    method: 'POST',
  }).success(function (data) {
    $scope.users = data;
  });
    console.log($scope.users); 

});
myapp.controller('sentCtrl', function($scope, $http) {
  $scope.name = 'aljim salem';
  $scope.users = [];
     
  $http({
    url: 'http://localhost/mysite_al/app/getMsg',
    method: 'POST',
  }).success(function (data) {
    $scope.users = data;
  });
    console.log($scope.users); 

});



myapp.controller('TabsDemoCtrl', function($scope, $window){
    
      $scope.goToInbox = function() {
        $window.location.href = "http://localhost/mysite_al/app/dashboard#/";
      }

      $scope.goToSent = function() {
        $window.location.href = "http://localhost/mysite_al/app/dashboard#/sent";
      }

      $scope.goToMsg = function() {
        $window.location.href = "http://localhost/mysite_al/app/dashboard#/message";
        //alert("hello");
      }
      


      $scope.oneAtATime = true;
      $scope.styles = 'navigator';
      $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
      };
});

