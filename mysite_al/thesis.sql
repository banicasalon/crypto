-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2015 at 12:25 PM
-- Server version: 10.0.12-MariaDB
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thesis`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_e` varchar(50) NOT NULL,
  `sub` varchar(250) NOT NULL,
  `msg` varchar(250) NOT NULL,
  `from_e` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `to_e`, `sub`, `msg`, `from_e`) VALUES
(1, 'carlotuma@gmail.com', 'sss', 'ss', 'carlotumala@gmail.com'),
(2, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(3, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(4, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(5, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(6, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(7, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(8, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(9, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(10, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(11, 'carlotuma@gmail.com', 'sample', 'sample', 'carlotumala@gmail.com'),
(12, 'carlotuma@gmail.com', 'admin', 'adsdasda', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(50) NOT NULL,
  `m_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `f_name`, `m_name`, `l_name`, `email`, `password`, `status`) VALUES
(3, 'carlo', 'tagupa', 'tumala', 'carlotumala@gmail.com', '123', '1'),
(10, 'aljim', 'salem', '123', 'admin', 'admin', '1'),
(11, 'aljim', 'ramos', 'salem', 'aljimsalem@gmail.com', '123', '1'),
(14, 'Banica', 'Palapar', 'Salon', 'banicasalon@gmail.com', '1234', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
